let users = [
    {
        userId: 1,
        email: "user@gmail.com",
        password: "12345",
        phone: "0123787323",
        address: "Ha Noi, Viet Nam",
        fullname: "Nguyen Van A",
        role: "USER",
        isDeleteFlag: false,

        cart: {
            cartId: 1,
            products: [
                {
                    productId: 1,
                    productName: "Nike Shoe No.1",
                    size:29,
                    price: 500.11,
                    quantity: 1,
                    favorite: 4,
                    status: "best seller",
                    manufacturer: "Nike",
                    categoryId: 1,
                },
                
            ]
        },
        boughtProducts: [
            {
            productId: 2,
            productName: "Convert Shoe No.1",
            size:27,
            price: 400.15,
            quantity: 2,
            favorite: 5,
            status: "",
            manufacturer: "Convert",
            categoryId: 2,
        },
        {
            productId: 3,
            productName: "Convert Shoe No.1",
            size:28,
            price: 400.15,
            quantity: 1,
            favorite: 5,
            status: "",
            manufacturer: "Convert",
            categoryId: 2,
        },

        
    ]
    }
]